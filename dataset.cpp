#include "dataset.h"
#include <cstdio>
#include <stdlib.h>
#include <fstream>
#include <iostream>

using namespace std;

void DataSet::Add(int populationCount,int reproductionCount, int deathCount)
{
  DataSetRow row;
  row.populationCount = populationCount;
  row.reproductionCount = reproductionCount;
  row.deathCount = deathCount;
  data.push_back(row);
}

void DataSet::Print()
{
  printf("%10s%10s%10s\n","Pop","Birth","Death");
  for(vector<DataSetRow>::iterator it = data.begin(); it != data.end(); ++it)
    {
      printf("%10d%10d%10d\n",
	     it->populationCount,
	     it->reproductionCount,
	     it->deathCount);
    }
}

void DataSet::WriteToFile()
{
  ofstream dataFile;
  dataFile.open("data.txt");
  if(dataFile.is_open())
    {
      int timePeriod=0;
      for(vector<DataSetRow>::iterator it = data.begin(); it != data.end(); ++it)
	{
	  dataFile << timePeriod << "\t"
		   << it->populationCount << "\t"
		   << it->reproductionCount << "\t"
		   << it->deathCount << "\n";
	  timePeriod++;
	}
    }
  else
    {
      cout << "File was not opened";
    }
  dataFile.close();
  
}

void DataSet::Plot()
{
  FILE *gnuplotPipe = popen("gnuplot","w");
  if(gnuplotPipe)
    {
      fprintf(gnuplotPipe,"set terminal png \n");
      fprintf(gnuplotPipe,"set output 'plot.png' \n");
      fprintf(gnuplotPipe,"plot 'data.txt' using 1:2 \n");
      fflush(gnuplotPipe);
      pclose(gnuplotPipe);
    }
  else
    {
      cout << "Failed to open pipe to gnuplot.\n";
    }
  system("display plot.png");
  
}

