#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include "dataset.h"
#include "organism.h"

using namespace std;

class Model
{
 public:
  // Constructor
  Model();

  // Mutator
  DataSet Run(int totalTime,int totalEnergy);

 private:  
  vector<Organism> population;
 
};

#endif
