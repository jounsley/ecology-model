#include "organism.h"

using namespace std;

const int Organism::REPRODUCTION_THRESHOLD = 5;
const int Organism::LIFESPAN = 20;

Organism::Organism()
{
  energy = 0;
  age = 0;
}

void Organism::Consume(int _energy)
{
  energy += _energy;
}

void Organism::Age()
{
  age ++;
  
}

bool Organism::Reproduce()
{
  if( energy >= REPRODUCTION_THRESHOLD)
    {
      energy = 0;
      return true;
    }
  return false;
}

bool Organism::Expire()
{
  return age >= LIFESPAN;
}
