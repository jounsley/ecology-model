#ifndef ORGANISM_H
#define ORGANISM_H

using namespace std;

class Organism
{
 private:
  static const int REPRODUCTION_PROBABILITY;
  static const int REPRODUCTION_THRESHOLD;
  static const int LIFESPAN;
    
  int energy;
  int age;

 public:
  // Constructor
  Organism();

  // Mutators
  void Consume(int energy);
  void Age();

  // State
  bool Reproduce();
  bool Expire();
  
};


#endif
