#include "model.h"
#include <time.h>
#include <cstdlib>

using namespace std;
Model::Model()
{
  Organism o;
  population.push_back(o);
}

DataSet Model::Run(int totalTime, int totalEnergy)
{
  DataSet data;
  
  for(int timeStep = 0; timeStep < totalTime; timeStep++)
    {
      int populationCount = 0;
      int deathCount = 0;
      int reproductionCount = 0;
      
      // initialise random seed
      srand(time(NULL));

      // distribute energy
      for(int energyCount = 0; energyCount < totalEnergy; energyCount++)
	{
	  int index = rand() % population.size();
	  population[index].Consume(1);
	}

      // check for expiration and reproduction
      // and age all organisms 
      for(vector<Organism>::iterator it = population.begin();
	  it != population.end(); ++it)
	{
	  if(!it->Expire())
	    {
	      populationCount++;
	      if(it->Reproduce())
		{
		  reproductionCount++;
		}
	      it->Age();
	    }else
	    {
	      // Erase the expired entry
	      it = population.erase(it);
	      deathCount++;
	    }
	}

      // add new organisms as determined by 
      // reporduction count
      for(int newOrganisms = 0;
	  newOrganisms < reproductionCount;
	  newOrganisms++)
	{
	  Organism o;
	  population.push_back(o);
	}
      data.Add( populationCount,reproductionCount,deathCount);
    }
  return data;
}
