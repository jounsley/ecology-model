OBJ = main.o model.o organism.o dataset.o display.o
CXX = g++
GTKFLAGS = `pkg-config --cflags --libs gtk+-2.0`
CXXFLAGS = -Wall $(GTKFLAGS)

main:$(OBJ)
	g++ -o main $(OBJ) $(CXXFLAGS)

clean:
	rm *.o