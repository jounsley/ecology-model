#ifndef DATASET_H
#define DATASET_H

#include <vector>

using namespace std;

class DataSet
{
 public:
  void Add(int populationCount, int reproductionCount, int deathCount);
  void Print();
  void WriteToFile();
  void Plot();

 private:
  struct DataSetRow
  {
    int populationCount, reproductionCount, deathCount;
  };
  vector<DataSetRow> data;

};

#endif
