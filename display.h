#ifndef DISPLAY_H
#define DISPLAY_H

#include <gtk/gtk.h>

using namespace std;

class Display
{
 public:
  Display(int argc, char *argv[]);
  void Run();
 private:
    GtkWidget *window;
    GtkWidget *button;
    
    static void hello(GtkWidget *widget, gpointer data)
    {
      g_print("Hello World\n");
    }
    
    static gboolean delete_event(GtkWidget *widget,
					  GdkEvent *event,
					  gpointer data)
      {
	g_print("Delete event occured/n");
	return TRUE;
      }
    
    static void destroy(GtkWidget *widget,
		    gpointer data)
    {
      gtk_main_quit();
    }
};

#endif
