#include "model.h"
#include "dataset.h"
#include "display.h"
#include <gtk/gtk.h>

using namespace std;



int main(int argc,
	 char *argv[])
{
  Model model;
  DataSet data = model.Run(100,1000);
  data.Print();
  data.WriteToFile();
  data.Plot();

  Display display(argc,argv);
  display.Run();
  

  return 0;
}
